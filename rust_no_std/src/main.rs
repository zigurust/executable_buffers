#![no_std]
#![no_main]

use core::panic::PanicInfo;

#[panic_handler]
fn panic(_panic_info: &PanicInfo) -> ! {
    loop {
    }
}

#[no_mangle] // this attribute disables the name mangling
pub extern "C" fn _start() -> ! {
   loop {}
}

