MEMORY
{
  rom      (rx)  : ORIGIN = 0x00000000, LENGTH = 1M
  ram      (rw)  : ORIGIN = 0x00100000, LENGTH = 1M
  xram     (rwx) : ORIGIN = 0x00200000, LENGTH = 1M
}

SECTIONS
{
  .text : {
      *(.text)
      *(.text*)
  } > rom
  .data : {
      *(.data)
      *(.data*)
  } > ram
  .modulecode : {
      *(.modulecode)
  }
}
