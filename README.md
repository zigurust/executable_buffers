# Executable buffers

Sample code for executing binary code from Rust and Zig.

## References

* [https://github.com/skerkour/black-hat-rust/blob/main/ch_08/executor/src/main.rs]
* [https://stackoverflow.com/questions/18476002/execute-binary-machine-code-from-c]
* [https://stackoverflow.com/questions/9960721/how-to-get-c-code-to-execute-hex-machine-code]
* [https://stackoverflow.com/questions/55856247/how-to-execute-raw-instructions-from-a-memory-buffer-in-rust#55859398]
* [https://github.com/bytecodealliance/wasmtime/blob/main/crates/wasmtime/src/module.rs#L435]
* [https://github.com/bytecodealliance/wasmtime/blob/main/crates/jit/src/code_memory.rs#L162]
* [https://github.com/bytecodealliance/wasmtime/blob/90876f717dbf0a23894d1e3f566b98c90a9cf685/crates/runtime/src/mmap.rs]
* [https://crates.io/crates/region]
* [https://allthingsembedded.com/post/2020-04-11-mastering-the-gnu-linker-script/]
* [https://mcyoung.xyz/2021/06/01/linker-script/]
* [https://mcyoung.xyz/2021/06/01/linker-script/]
* [https://interrupt.memfault.com/blog/how-to-write-linker-scripts-for-firmware]
* [https://stackoverflow.com/questions/28688484/actual-default-linker-script-and-settings-gcc-uses]
* [https://docs.rust-embedded.org/embedonomicon/memory-layout.html]
* [https://blog.knoldus.com/os-in-rust-an-executable-that-runs-on-bare-metal-part-1/]
* [https://blog.knoldus.com/os-in-rust-an-executable-that-runs-on-bare-metal-part-2/]
* [https://os.phil-opp.com/freestanding-rust-binary/]
* [https://doc.rust-lang.org/1.16.0/book/no-stdlib.html]
* [https://www.reddit.com/r/rust/comments/dii56a/trying_to_figure_out_how_to_cast_to_a_function_in/]

## License

[MIT](./LICENSE)

