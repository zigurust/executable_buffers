const RET0_CODE: [u8;3] = [
    0x31, 0xC0, // xor eax, eax
    0xC3 // ret
];

#[link_section = ".text"]
static ADD_CODE: [u8;4] = [
    0x8D, 0x04, 0x37, //  lea eax,[rdi+rsi] (retval = a+b)
    0xC3 //  ret   
];

#[link_section = ".data"]
static mut MODULE_MEMORY: [u8;3] = [0;3];

#[link_section = ".module_code"]
static mut MODULE_CODE: [u8;10] = [RET0_CODE[0], RET0_CODE[1], RET0_CODE[2], 0, 0, 0, 0, 0, 0, 0];

use std::mem;

fn main() {

    unsafe {
        MODULE_MEMORY[0] = 0x31;
        MODULE_MEMORY[1] = 0xC0;
        MODULE_MEMORY[2] = 0xC3;
    }
    let mut stack:[u8;3] = unsafe  {[RET0_CODE[0], RET0_CODE[1], RET0_CODE[2]]};
    unsafe {
        println!("module memory: {MODULE_MEMORY:?}");
        println!("module code: {MODULE_CODE:?}");
    }

    let ret0: extern "C" fn() -> isize = unsafe {
        mem::transmute(&stack)
    };
    let ret = ret0();
    println!("return: {ret}");

    let add: fn(isize,isize) -> isize = unsafe {
        mem::transmute(&ADD_CODE)
    };
    let sum = add(4,5);
    println!("4+5={sum}");
}
