/*
OUTPUT_FORMAT(elf64-x86-64)
OUTPUT_ARCH(i386:x86-64)
*/

MEMORY
{
  rom      (rx)  : ORIGIN = 0x00000000, LENGTH = 1M
  ram      (rw)  : ORIGIN = 0x00100000, LENGTH = 1M
  xram     (rwx) : ORIGIN = 0x00200000, LENGTH = 1M
}

PHDRS
{
  headers PT_PHDR PHDRS ;
  interp PT_INTERP ;
  text PT_LOAD FILEHDR PHDRS ;
  data PT_LOAD ;
  dynamic PT_DYNAMIC ;
}

SECTIONS
{

  . = SIZEOF_HEADERS;
  
  .interp : { *(.interp) } :text :interp
  .text : { *(.text) } :text
  .rodata : { *(.rodata) }
  . = . + 0x1000;
  .data : { *(.data) } :data
  .dynamic : { *(.dynamic) } :data :dynamic

  .module_code : { *(.module_code*) } > xram
}

